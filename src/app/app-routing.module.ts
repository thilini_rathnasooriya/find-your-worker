import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SignupComponent } from './component/signup/signup.component';
import { LoginComponent } from './component/login/login.component';
import { FindComponent} from './component/find/find.component';
import { HomeComponent } from './component/home/home.component';
import { MapComponent } from './component/map/map.component';
import { LaterComponent} from './component/later/later.component';
import { PendingComponent } from './component/pending/pending.component';
import { NowComponent } from './component/now/now.component';
import { WorkerProfileComponent } from './component/worker-profile/worker-profile.component';
import { ClientProfileComponent } from './component/client-profile/client-profile.component';
import { WorkerRequestComponent} from  './component/worker-request/worker-request.component';


const routes: Routes = [
   {
    path: '',
    component: HomeComponent
  },
  {
    path: 'signup',
    component: SignupComponent
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'find',
    component: FindComponent
  },
  {
    path: 'map',
    component: MapComponent
  },
  {
    path: 'later',
    component: LaterComponent
  },
  {
    path:'pending',
    component: PendingComponent
  
  },
  {
    path:'now',
    component: NowComponent
  },
  {
    path:'worker-profile',
    component: WorkerProfileComponent
  },
  {
    path:'client-profile',
    component: ClientProfileComponent
  },
  {
   path:'worker-request',
   component: WorkerRequestComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
