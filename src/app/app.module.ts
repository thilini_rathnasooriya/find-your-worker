
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';

import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LoginComponent } from './component/login/login.component';
// import { RegisterComponent } from './components/register/register.component';
import { SignupComponent } from './component/signup/signup.component';
import { AppRoutingModule } from './app-routing.module';
import { NavComponent } from './component/nav/nav.component';
import { FindComponent } from './component/find/find.component';
import { HomeComponent } from './component/home/home.component';
import { MapComponent } from './component/map/map.component';
import { AgmCoreModule } from '@agm/core';
import { LaterComponent } from './component/later/later.component';
import { PendingComponent } from './component/pending/pending.component';
import { NowComponent } from './component/now/now.component';
import { WorkerProfileComponent } from './component/worker-profile/worker-profile.component';
import { UserService } from './shared/user.service';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { ClientProfileComponent } from './component/client-profile/client-profile.component';
import { WorkerRequestComponent } from './component/worker-request/worker-request.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    // RegisterComponent,
    SignupComponent,
    NavComponent,
    FindComponent,
    HomeComponent,
    MapComponent,
    LaterComponent,
    PendingComponent,
    NowComponent,
    WorkerProfileComponent,
    ClientProfileComponent,
    WorkerRequestComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MDBBootstrapModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyB8lgLyWZZBgmCV6EAVADrIanxU03jXfWc'
    }),
    HttpClientModule,
    ToastrModule.forRoot()
  ],
  providers: [UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
    