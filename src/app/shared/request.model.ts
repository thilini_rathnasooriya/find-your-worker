export interface RequestModel {
    RequestId: number;
    WorkerId: number;
    CreatedDate: any;
    ClientId: number;
    StartTime: string;
    ExpectedEndTime: string;
    OrderDate: string;
    OrderLocation: string;
    SkillId: number;
    SkillTitle: string;
}

export interface RequestResponseModel {
    status: number;
    result: Array<Array<RequestModel>>;
    message: string;
}