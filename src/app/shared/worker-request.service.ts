import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/internal/Observable';
import { RequestResponseModel } from './request.model';



@Injectable({
  providedIn: 'root'
})
export class WorkerRequestService {

  constructor(private http: HttpClient) { }

  getRequests(workerId): Observable<RequestResponseModel>{
    return this.http.get<RequestResponseModel>('http://localhost:3000/requests/pool/worker/' + workerId);
  }

  acceptRequest(requestId, workerId): Observable<any>{
    return this.http.post<any>('http://localhost:3000/requests/accept/' + requestId, {
      'WorkerId': workerId
    })
  }
}