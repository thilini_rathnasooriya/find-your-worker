export interface DetailsModel {
    FirstName: string;
    LastName: string;
    BaseLocation: string;
    ImgUrl: string;
    ContactNumber: string;
    UserEmail: string;
}

export interface ProfileModel {
    status: number;
    result: {
        recordsets: Array<Array<DetailsModel>>
    }
    message: string;
}