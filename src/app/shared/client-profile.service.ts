import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { HttpClient } from '@angular/common/http';
import { ProfileModel} from './profile.model';

@Injectable({
  providedIn: 'root'
})
export class ClientProfileService {

  constructor( 
    private http: HttpClient
  ) { }

 getProfile(clientId):Observable<ProfileModel>{
    return this.http.get<ProfileModel>('http://localhost:3000/client/profile/' + clientId);
  }

  insertProfile(email, fname, lname, baseL,imgURL, contactno):Observable<ProfileModel>{
    return this.http.post<ProfileModel>('http://localhost:3000/client/profile', {
      'email': email, 'fname': fname, 'lname':lname, 'baseL': baseL, 'ingURL': imgURL, 'contactno': contactno
    });
  }

  updateProfile(clientId, fname, lname, baseL, imgURL, contactno):Observable<ProfileModel>{
    return this.http.put<ProfileModel>('http://localhost:3000/client/profile/' + clientId, {
      'fname': fname, 'lname': lname, 'baseL': baseL, 'imgURL': imgURL, 'contactno': contactno
    });
  }
}