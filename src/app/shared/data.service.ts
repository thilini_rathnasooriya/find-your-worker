import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { SkillModelRes } from './skill.model';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor( private http: HttpClient) { }

  getAllSKills() {
    return this.http.get<SkillModelRes>('http://localhost:3000/dataservices/getallskills');
  }

  getAllLocations(): Observable<any> {
    return this.http.get<any>('http://localhost:3000/dataservices/getalllocations');
  }
}