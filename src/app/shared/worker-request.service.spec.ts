import { TestBed } from '@angular/core/testing';

import { WorkerRequestService } from './worker-request.service';

describe('WorkerRequestService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: WorkerRequestService = TestBed.get(WorkerRequestService);
    expect(service).toBeTruthy();
  });
});
