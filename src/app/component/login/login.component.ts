import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { UserService } from 'src/app/shared/user.service';
import { ToastrService } from 'ngx-toastr';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  form;

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private toastr: ToastrService,
    private router: Router
  ) {
    this.form = this.fb.group({
      username: ['', [
        Validators.required,
        Validators.email
      ]],
      password: ['', Validators.required]
    });
  }

  loginSubmit() {
    if (this.username.value === "" || this.password.value === "") {
      this.toastr.warning("Enter your username and password");
    }
    else {
      this.userService.loginUser(this.username.value, this.password.value).subscribe(
        result => {
          if (result.status === 200 && result.message === 'Authorized') {
            this.toastr.success('Login Success');
            localStorage.setItem('sessionEmail', result.result.sessionEmail);
            localStorage.setItem('sessionType', result.result.sessionType);
            localStorage.setItem('UserId', result.result.UserId);

            this.userService.headerStateChange.next(true);

            if (result.result.sessionType === 'worker') {
              this.router.navigate(['worker-request']);
            } else {
              this.router.navigate(['client-profile']);
            }
          } else {
            this.toastr.error('Login Failed : Check your username and password)');
          }
        }
      );
    }

  }

  ngOnInit() {
  }

  get username() { return this.form.get('username'); }
  get password() { return this.form.get('password'); }

}
