import { Component, OnInit } from '@angular/core';
import { RequestModel } from '../../shared/request.model';
import { WorkerRequestService } from '../../shared/worker-request.service';
import { ActivatedRoute, Router} from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { UserService } from '../../shared/user.service';
import { SkillModel } from '../../shared/skill.model';
import { DataService } from '../../shared/data.service';


@Component({
  selector: 'app-worker-request',
  templateUrl: './worker-request.component.html',
  styleUrls: ['./worker-request.component.scss']
})
export class WorkerRequestComponent implements OnInit {

  RequestList: RequestModel[] = [];
  StartTime: string;
  ExpectedEndTime: string;
  OrderDate: string;
  OrderLocation: string;
  SkillId: number;
  SkillList: SkillModel[];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private toastrService: ToastrService,
    private userService: UserService,
    private dataService: DataService,
    private requestService: WorkerRequestService
  ) { }

  ngOnInit() {

    this.dataService.getAllSKills().subscribe(
      res => {
        this.SkillList = res.recordset;
        this.requestService.getRequests(localStorage.getItem('UserId')).subscribe(
          res => {
            this.RequestList = res.result[0];
            console.log(this.RequestList);
            for (const request of this.RequestList) {
              for (const skill of this.SkillList) {
                if (request.SkillId === skill.SkillId) {
                  request.SkillTitle = skill.SkillTitle;
                }
              }
            }
          },
        );
      }
    );

  }

  acceptRequest(requestId){
    this.requestService.acceptRequest(requestId, localStorage.getItem('UserId')).subscribe(
      res => {
        console.log(res);
        this.router.navigate(['/worker/orders']);
      }
    )
  }

}
