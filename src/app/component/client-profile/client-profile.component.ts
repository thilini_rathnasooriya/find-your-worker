import { Component, OnInit } from '@angular/core';
import { ProfileModel } from '../../shared/profile.model';
import { ClientProfileService } from 'src/app/shared/client-profile.service';
import { UserService } from '../../shared/user.service';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'src/app/shared/data.service';

@Component({
  selector: 'app-client-profile',
  templateUrl: './client-profile.component.html',
  styleUrls: ['./client-profile.component.scss']
})
export class ClientProfileComponent implements OnInit {

  info: ProfileModel;
  Email: string;
  FirstName: string;
  LastName: string;
  ImgUrl: string;
  ContactNumber:  string;
  BaseLocation: string;
  locations = [];


  // isLogged: boolean;
  isLogged;
  isClient;

  constructor(
    private clientProfileService: ClientProfileService,
    private userService: UserService,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private router: Router,
    private dataService: DataService,

  ) { 

   
   }

  ngOnInit() {
    this.clientProfileService.getProfile(localStorage.getItem('UserId')).subscribe(
      res => {
        console.log(res);
        this.info = res;
        if(this.info.result.recordsets[0][0].ImgUrl===null){
          this.ImgUrl = "../../../../assets/images/profile.png";
        }
        else{
          this.ImgUrl = this.info.result.recordsets[0][0].ImgUrl;
        }
        if(
          this.info.result.recordsets[0][0].FirstName === null 
          ){
          this.FirstName = ""
        }
        else{
          this.FirstName = this.info.result.recordsets[0][0].FirstName;
        }
        if(
          this.info.result.recordsets[0][0].LastName===null
         
        ){
          this.LastName = ""
        }
        else{
          this.LastName = this.info.result.recordsets[0][0].LastName;
        }
        if( 
          this.info.result.recordsets[0][0].BaseLocation===null
        ){
          this.BaseLocation = ""
        }
        else{
         this.BaseLocation = this.info.result.recordsets[0][0].BaseLocation;  
        }

        this.Email = this.info.result.recordsets[0][0].UserEmail;
        this.ContactNumber = this.info.result.recordsets[0][0].ContactNumber;  
      }
    );

    this.dataService.getAllLocations().subscribe(
      res => {
        this.locations = res.recordset;
      }
    );

    if (this.userService.getUserType() === 'Client') {
      this.isClient = true;
    }
    else {
      this.isClient = false;
    }
  }

  updateProfile(){
    this.clientProfileService.updateProfile(localStorage.getItem('UserId'), this.FirstName, this.LastName, this.BaseLocation, this.ImgUrl, this.ContactNumber).subscribe(
      res => {
        console.log(res);
        if (res.status === 200) {
          this.toastr.success('successfuly updated');
        } else {
          this.toastr.error('Update failed');
        }
      }
      
    )
  }

}