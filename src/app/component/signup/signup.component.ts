import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup,FormBuilder, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { ConfirmPasswordValidator } from '../../validators/confirmpassword.validator';
import { UserService } from '../../shared/user.service';


@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  form; FormGroup;
  formSubmitted = false;

  constructor(
    private fb: FormBuilder,
    private userService: UserService,
    private router: Router,
    private toastr: ToastrService
  ) {
    this .form = this .fb .group({
      username: ['', [
        Validators.required,
        Validators.email
      ]],
      password: ['', [
        Validators.required,
        Validators.minLength(8)
      ]],
      confirmPassword: ['', [
        Validators.required,
        ConfirmPasswordValidator('password')
      ]],
      contactNumber: ['', [
        Validators.required, 
        Validators.minLength(10), 
        Validators.maxLength(10)
      ]],
      userType: ['', Validators.required]
    });
  }
  //When the form is submitted,set the this.formSubmitted field to true
  
  onSubmit(event) {
    event.preventDefault();
    this.formSubmitted = true;

    //then check if the form is valid before logging it to the console (you process the form and send it to the server)
    if (this.form.valid) {
      console.log(this.form.value); 
    }
  }
    checkValid() {
      if(this.form.get('username').valid && this.form.get('password').valid && this.form.get('confirmPassword').valid && this.form.get('contactNumber').valid && this.form.get('userType').valid) {
        return false;
      } else {
        return true;
      }
    }
  
  
  ngOnInit() {
    
  }

  registerSubmit(form) {
    console.log(form.value.username);
    this.userService.registerUser(form.value.username, form.value.password, form.value.contactNumber, form.value.userType).subscribe(
      result => {
        console.log(result);
        if (result.status === 201) {
          this.toastr.success('Registerd Succesfully');
          this.router.navigate(['login']);
        } else {
          this.toastr.error('User registration Failed', result.message);
        }
      }
    );
  }

  //To be able to check the status of each form-field, To do that, defining some getters in our component
  get username() {
    return this.form.get('username');
  }

  get password() {
    return this.form.get('password');
  }

  get confirmPassword() {
    return this.form.get('confirmPassword');
  }

  get contactNumber() {
    return this.form.get('contactNumber');
  }

}