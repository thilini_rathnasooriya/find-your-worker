import { Component, OnInit } from '@angular/core';
import { UserService } from '../../shared/user.service';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  isLogged: boolean;
  subscription: Subscription;
  message: string;

  constructor(
    private userservice: UserService,
    private router: Router
  ) { }

  ngOnInit() {
    // this.isLogged = this.userservice.isLogged();
    // console.log(this.isLogged);
    this.userservice.headerStateChange.subscribe(
      res => {
        this.isLogged = res;
      }
    );
  }

  navigateToProfile() {
    if (this.userservice.getUserType() === 'worker') {
      this.router.navigate(['worker-profile']);
    } else {
      this.router.navigate(['client-profile']);
    }
  }

  logout() {
    this.userservice.logout();
    localStorage.removeItem('sessionEmail');
    localStorage.removeItem('sessionType');
    this.userservice.headerStateChange.next(false);
    this.router.navigate(['']);
  }

}
